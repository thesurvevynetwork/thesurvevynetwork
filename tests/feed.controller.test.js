var people = [];

describe('profile', function() {
		beforeEach(module('survevyNetwork'));
		
		var $controller, dataFactory;
		
		beforeEach(inject(function(_$controller_, _dataFactory_) {
			$controller = _$controller_;
			dataFactory = _dataFactory_;
		}));
		
		describe('posts are in the right order', function() {
			it('posts are ordered by date', function () {
				var $scope = {};
				var controller = $controller('FeedController', {$scope: $scope});
				var posts = $scope.friendsPosts;
                
                expect(posts).toEqual([
                			{ text: 'This is a post', poster: 'Mike Kiley', pic: 'https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png', date: 5, likeCount: 10, comments: [{ text: 'First Comment', date: 958759242, likeCount: 2 }, { text: 'Second Comment', date: 743140042, likeCount: 3 } ] }, 
                			{ text: 'Evans POST', poster: 'Evan Altman', pic: 'https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Penguin-512.png', date: 4, likeCount: 10, comments: [  ] }, 
                			{ text: 'MARKS POST', poster: 'Mark Powers', pic: 'https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Panda-512.png', date: 3, likeCount: 10, comments: [  ] }, 
                			{ text: 'This is a 2 post', poster: 'Mike Kiley', pic: 'https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png', date: 2, likeCount: 10, comments: [{ text: 'First Comment', poster: 'Mike Kiley', pic: 'https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png', date: 1463680938, likeCount: 2 }, { text: 'Second Comment', date: 643140042, likeCount: 3 } ] },
                			{ text: 'This is a 3 post', poster: 'Mike Kiley', pic: 'https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png', date: 1, likeCount: 10, comments: [ { text: 'First Comment', date: 643140042, likeCount: 2 }, { text: 'Second Comment', date: 843140042, likeCount: 3 }]}]);
            });
        });
		
});
