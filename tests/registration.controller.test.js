describe('login', function() {
		beforeEach(module('survevyNetwork'));
		
		var $controller, dataFactory, $location;
		
		beforeEach(inject(function(_$controller_, _dataFactory_, _$location_) {
			$controller = _$controller_;
			dataFactory = _dataFactory_;
            $location = _$location_;
            spyOn(_$location_, "path");
		}));
		
		
		describe('check successful registration', function() {
			it('Registering with valid username and password should create user', function () {
				var $scope = {};
				var controller = $controller('registrationControl', {$scope: $scope});
				
				$scope.newUsername = "MikeJipping";
		        $scope.newPassword = "iosbro1111";
		        $scope.newPasswordConfirm = "iosbro1111";
				$scope.makeNewUser();
				var users = dataFactory.getUsers();
                expect(users[3]).toEqual({username: "MikeJipping", password: "iosbro1111", realName: "MikeJipping", pictureUrl:"https://cdn3.iconfinder.com/data/icons/round-icons-3/137/roundicons_free_set-05-512.png", bioText: "No bio given", posts: []})
			});
        });
    describe('check unsuccessful registration', function() {
            it('Cannot register same person twice', function () {
				var $scope = {};
				var controller = $controller('registrationControl', {$scope: $scope});
				
				$scope.newUsername = "MikeJipping";
		        $scope.newPassword = "iosbro1111";
		        $scope.newPasswordConfirm = "iosbro1111";
				$scope.makeNewUser();
                $scope.newUsername = "MikeJipping";
		        $scope.newPassword = "iosbro1111";
		        $scope.newPasswordConfirm = "iosbro1111";
				$scope.makeNewUser();
                var users = dataFactory.getUsers();
                expect(users.length).toEqual(4);
			});
            it("Cannot register with passwords that do not match", function () {
				var $scope = {};
				var controller = $controller('registrationControl', {$scope: $scope});
				
				$scope.newUsername = "MikeJipping";
		        $scope.newPassword = "iosbro1111";
		        $scope.newPasswordConfirm = "iosbro2222";
				$scope.makeNewUser();;
                var users = dataFactory.getUsers();
                expect(users.length).toEqual(3);
			});
        it("Registering with empty username and passwords does nothing", function () {
				var $scope = {};
				var controller = $controller('registrationControl', {$scope: $scope});
				
				$scope.newUsername = "";
		        $scope.newPassword = "";
		        $scope.newPasswordConfirm = "";
				$scope.makeNewUser();;
                var users = dataFactory.getUsers();
                expect(users.length).toEqual(3);
                expect($location.path).not.toHaveBeenCalled();
			});
            
		
});
});