describe('Date Formatter Tests', function() {
  	var $filter;

  	beforeEach(function () {
    	module('survevyNetwork');

    	inject(function (_$filter_) {
      		$filter = _$filter_;
    	});
  	});

  	describe('timeSinceDateFilter', function() {
    	it('should be the right format based on how long ago the given date was.', function() {   
    		// Invalid times
			expect($filter('timeSinceDateFilter')(NaN)).toBe("A while a go.");
			expect($filter('timeSinceDateFilter')(undefined)).toBe("A while a go.");
			expect($filter('timeSinceDateFilter')(10001, 10000)).toBe("A while a go.");
			// Seconds
			expect($filter('timeSinceDateFilter')(10000, 10000)).toBe("0 seconds ago.");
			expect($filter('timeSinceDateFilter')(9999, 10000)).toBe("1 seconds ago.");
			expect($filter('timeSinceDateFilter')(9990, 10000)).toBe("10 seconds ago.");
			// Minutes
			expect($filter('timeSinceDateFilter')(9940, 10000)).toBe("1 minutes ago.");
			expect($filter('timeSinceDateFilter')(9900, 10000)).toBe("1 minutes ago.");
			expect($filter('timeSinceDateFilter')(9880, 10000)).toBe("2 minutes ago.");
			expect($filter('timeSinceDateFilter')(6401, 10000)).toBe("59 minutes ago.");
			// Hours
			expect($filter('timeSinceDateFilter')(6400, 10000)).toBe("1 hours ago.");
			expect($filter('timeSinceDateFilter')(2400, 10000)).toBe("2 hours ago.");
			expect($filter('timeSinceDateFilter')(1, 86400)).toBe("23 hours ago.");
			// Days
			expect($filter('timeSinceDateFilter')(1, 86401)).toBe("1 days ago.");
			expect($filter('timeSinceDateFilter')(1, 864010)).toBe("10 days ago.");
			expect($filter('timeSinceDateFilter')(1, 31536000)).toBe("364 days ago.");
			// Years
			expect($filter('timeSinceDateFilter')(1, 31536001)).toBe("1 years ago.");
			expect($filter('timeSinceDateFilter')(1, 615360010)).toBe("19 years ago.");
    	});
  	});
});
