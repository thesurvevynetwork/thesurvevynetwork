describe('Forgotten password tests', function() {
		beforeEach(module('survevyNetwork'));
		
		var $controller, $location, emailService;
		
		beforeEach(inject(function(_$controller_, _$location_) {
			$controller = _$controller_;
            $location = _$location_;
            spyOn(_$location_, "path");
		}));
		
		beforeEach(angular.mock.inject(function GetService(forgotPasswordEmailService) {
			emailService = forgotPasswordEmailService;
			spyOn(emailService,"sendEmail");
		}));
		
		it('should fire request to email service when submit is clicked.', function() {
			var $scope = {};
			var controller = $controller('forgotPasswordEmailControl',{$scope: $scope});
			
			$scope.emailAddress="test@yahoo.com";
			
			$scope.sendPasswordEmail();
			
			expect(emailService.sendEmail).toHaveBeenCalledWith("test@yahoo.com");
			expect($location.path).toHaveBeenCalledWith("/");
		});
});		
	