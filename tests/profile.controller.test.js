var people = [];

describe('profile', function() {
		beforeEach(module('survevyNetwork'));
		
		var $controller, dataFactory;
		
		beforeEach(inject(function(_$controller_, _dataFactory_) {
			$controller = _$controller_;
			dataFactory = _dataFactory_
            setPeople();
		}));
		
		describe('makePost() works correctly', function() {
			it('cannot post when viewing someone elses profile', function () {
				var $scope = {};
				var controller = $controller('ProfileController', {$scope: $scope});
				
				$scope.currentUser = people[0];
				$scope.profileUser = people[1];
                $scope.postText = "This is a new post";
				$scope.makePost();
				expect($scope.currentUser).toEqual(people[0]);
                expect($scope.profileUser).toEqual(people[1]);
			});
            it('new posts are right', function () {
				var $scope = {};
				var controller = $controller('ProfileController', {$scope: $scope});
				
                // Empty post does not make a new post
				$scope.currentUser = people[1];
				$scope.profileUser = people[1];
                $scope.postText = "";
				$scope.makePost();
				expect($scope.currentUser).toEqual(people[1]);
                expect($scope.profileUser).toEqual(people[1]);
                
                // Posts are prepended to the list.
                $scope.postText = "This is my most recent post";
				$scope.makePost();
                var oldPosts = [{text: "MARKS POST", date : "5/17/16", likeCount : 10, comments : []}];
                oldPosts.shift({text: $scope.postText, date : $scope.getDate(), likeCount : 0, comments : []});
                expect($scope.profileUser).toEqual(people[1]);
                
			});
		});
        describe('misc functions', function(){
            it('editing is only allowed when current user is the profile user', function(){
                var $scope ={};
                var controller = $controller('ProfileController', {$scope: $scope});
                
                $scope.currentUser = people[0];
                $scope.profileUser = people[2];
                // Profile is not editable on load
                expect($scope.editMode).toEqual(false);
                $scope.editProfile();
                expect($scope.editMode).toEqual(false);
                // Changing profile user to the current user
                $scope.profileUser = people[0];
                expect($scope.editMode).toEqual(false);
                $scope.editProfile();
                expect($scope.editMode).toEqual(true);
            });
            it('getPosts() returns the right persons posts', function(){
                var $scope = {};
				var controller = $controller('ProfileController', {$scope: $scope});
				
                $scope.currentUser = people[2];
				$scope.profileUser = people[1];
                var posts = $scope.getCurrentPosts();
                expect(posts).toEqual(people[1].posts);
            });
        })
		
});

function setPeople(){
    people = [{username: "mike", password: "jandyrules", realName: "Mike Kiley", pictureUrl: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png",
                  bioText : "Hey its me, Mike. I just love tennis. Livin' it up in Cook Village 232",
                  posts : [{text: "This is a post", date : "5/17/16", likeCount : 10,
                                comments : [{text : "First Comment", date : "Today", likeCount : 2},{text : "Second Comment", date : "5:00", likeCount : 3}]},
                           {text: "This is a 2 post", date : "5/17/16", likeCount : 10,
                                comments : [{text : "First Comment", date : "Today", likeCount : 2},{text : "Second Comment", date : "5:00", likeCount : 3}]},
                           {text: "This is a 3 post", date : "5/17/16", likeCount : 10,
                                comments : [{text : "First Comment", date : "Today", likeCount : 2},{text : "Second Comment", date : "5:00", likeCount : 3}]}]},
				 {username: "mark", password: "powers", realName: "Mark Powers", pictureUrl: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Panda-512.png",
                    bioText : "Hey im mark powers, Im just writing this to test it out",
                  posts : [{text: "MARKS POST", date : "5/17/16", likeCount : 10, comments : []}]},
				 {username: "evan", password: "one", realName : "Evan Altman", pictureUrl:"https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Penguin-512.png",
                  posts : [{text: "Evans POST", date : "5/17/16", likeCount : 10, comments : []}]}];
}