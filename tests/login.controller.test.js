describe('Login tests', function() {
		beforeEach(module('survevyNetwork'));
		
		var $controller, dataFactory, $location;
		
		beforeEach(inject(function(_$controller_, _dataFactory_, _$location_) {
			$controller = _$controller_;
			dataFactory = _dataFactory_;
            $location = _$location_;
            spyOn(_$location_, "path");
		}));
		
		
		describe('check logged in user', function() {
			it('Logging in as evan should log evan in', function () {
				var $scope = {};
				var controller = $controller('loginController', {$scope: $scope});
				
				$scope.currUsername = "evan";
				$scope.currPassword = "one";
				$scope.login();
				var user = dataFactory.getCurrentUser();
				expect(user).toEqual({username: "evan", password: "one", realName : "Evan Altman", pictureUrl:"https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Penguin-512.png",
                  posts : [{text: "Evans POST", poster: 'Evan Altman', pic: 'https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Penguin-512.png', date : 4, likeCount : 10, comments : []}]});
			});
            it("check that logging in as evan takes you to evans feed", function () {
				var $scope = {};
				var controller = $controller('loginController', {$scope: $scope});
				
				$scope.currUsername = "evan";
				$scope.currPassword = "one";
				$scope.login();
                expect($location.path).toHaveBeenCalledWith("/feed");
                
			});
		});
    
        describe('check that login service does not login unregistered users', function() {
			it('check that clicking login with blank username and password does not log you in', function () {
				var $scope = {};
				var controller = $controller('loginController', {$scope: $scope});
				
				$scope.currUsername = "";
				$scope.currPassword = "";
				$scope.login();
				var user = dataFactory.getCurrentUser();
				expect(user).toEqual(undefined);
                expect($location.path).not.toHaveBeenCalled();
                
			});
            it('check login with correct username and incorrect password ', function () {
				var $scope = {};
				var controller = $controller('loginController', {$scope: $scope});
				
				$scope.currUsername = "evan";
				$scope.currPassword = "two";
				$scope.login();
				var user = dataFactory.getCurrentUser();
				expect(user).toEqual(undefined);
                expect($location.path).not.toHaveBeenCalled();
                
			});
            it('check login with incorrect username and correct password ', function () {
				var $scope = {};
				var controller = $controller('loginController', {$scope: $scope});
				
				$scope.currUsername = "mike";
				$scope.currPassword = "one";
				$scope.login();
				var user = dataFactory.getCurrentUser();
				expect(user).toEqual(undefined);
                expect($location.path).not.toHaveBeenCalled();
                
			});
            it('check that clicking login with blank username and password does not log you in', function () {
				var $scope = {};
				var controller = $controller('loginController', {$scope: $scope});
				
				$scope.currUsername = "";
				$scope.currPassword = "";
				$scope.login();
				var user = dataFactory.getCurrentUser();
				expect(user).toEqual(undefined);
                expect($location.path).not.toHaveBeenCalled();
                
			});
		});		
});