describe('survevy network service test', function () {
	beforeEach(angular.mock.module('survevyNetwork'));
	
	var service, testUsers;
		
	//Reset the service before each test	
	beforeEach(angular.mock.inject(function GetService(dataFactory) {
		service = dataFactory;
	}));
	
	beforeEach(function() {
		testUsers = [{username: "TestUser1", password: "TestPass", realName: "Test User1", pictureUrl: "nothing.jpg",
                  bioText : "I am a test",
                  posts : [{text: "This is a post", date : "5/17/16", likeCount : 10,
                                comments : [{text : "First Comment", date : "Today", likeCount : 2}]}]},
				{username: "TestUser2", password: "TestPass", realName: "Test User2", pictureUrl: "nothing.jpg",
                  bioText : "I am a test",
                  posts : [{text: "This is a post2", date : "5/17/16", likeCount : 10,
                                comments : [{text : "First Comment", date : "Today", likeCount : 2}]}]},
				{username: "TestUser3", password: "TestPass", realName: "Test User1", pictureUrl: "nothing.jpg",
                  bioText : "I am a test",
                  posts : [{text: "This is a post3", date : "5/17/16", likeCount : 10,
                                comments : [{text : "First Comment", date : "Today", likeCount : 2}]}]}];
								
		//Set the service's people to be the test people
		service.model.people = testUsers;
	});
		
	it('should start with current user as undefined', function() {
		expect(service.getCurrentUser()).toBe(undefined);
	});
	
	it('should return all users when getUsers() is called', function() {
		expect(service.getUsers()).toBe(testUsers);
	});
	
	it('should return no users if none are present', function() {
		service.model.people = [];
		expect(service.getUsers()).toEqual([]);
	});
	
	it('should get the usernames as strings from getUserNames', function() {
		expect(service.getUserNames()).toEqual(["TestUser1", "TestUser2","TestUser3"]);
	});
	
	it('should get an empty list of usernames if no users are present', function() {
		service.model.people = [];
		expect(service.getUserNames()).toEqual([]);
	});
	
	it('should set the current user', function() {
		var user = testUsers[0];
		
		service.setCurrentUser(user);
		
		expect(service.getCurrentUser()).toBe(user);
	});	
	
	it('should get the friends of the current user', function() {
		service.setCurrentUser(testUsers[0]);
		
		expect(service.getFriends()).toEqual([testUsers[1],testUsers[2]]);
	});
	
	it('should show no friends if there is only one user', function() {
		service.model.people = [testUsers[0]];
		service.setCurrentUser(testUsers[0])
		expect(service.getFriends()).toEqual([]);
	});
	
	//the addUser function was removed to make the registration controller be in charge of adding new users
	/* it('should allow for new users to be added', function() {
		var newUserInfo = {username: "testNewUser", password: "testNewUserPassword"};
								
		service.addUser(newUserInfo.username, newUserInfo.password);
		
		expect(service.model.people).toContain({username: newUserInfo.username, password: newUserInfo.password, realName: newUserInfo.username, pictureUrl: "https://cdn3.iconfinder.com/data/icons/round-icons-3/137/roundicons_free_set-05-512.png", 
                                      bioText: "No bio given", posts: []});
	}); */
	
});

