mainApp.factory('dataFactory', function($location) {
	var service = {};

	service.model = {
		people: [{username: "mike", password: "jandyrules", realName: "Mike Kiley", pictureUrl: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png",
                  bioText : "Hey its me, Mike. I just love tennis. Livin' it up in Cook Village 232",
                  posts : [{text: "This is a post", poster: "Mike Kiley", pic: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png",  date : 5, likeCount : 10,
                                comments : [{text : "First Comment", date : 958759242, likeCount : 2},{text : "Second Comment", date : 743140042, likeCount : 3}]},
                           {text: "This is a 2 post", poster: "Mike Kiley", pic: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png", date : 2, likeCount : 10,
                                comments : [{text : "First Comment", poster: "Mike Kiley", pic: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png", date : 1463680938, likeCount : 2},{text : "Second Comment", date : 643140042, likeCount : 3}]},
                           {text: "This is a 3 post", poster: "Mike Kiley", pic: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Cat-512.png", date : 1, likeCount : 10,
                                comments : [{text : "First Comment", date : 643140042, likeCount : 2},{text : "Second Comment", date : 843140042, likeCount : 3}]}]},
				 {username: "mark", password: "powers", realName: "Mark Powers", pictureUrl: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Panda-512.png",
                    bioText : "Hey im mark powers, Im just writing this to test it out",
                  posts : [{text: "MARKS POST", poster: "Mark Powers", pic: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Panda-512.png", date : 3, likeCount : 10, comments : []}]},
				 {username: "evan", password: "one", realName : "Evan Altman", pictureUrl:"https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Penguin-512.png",
                  posts : [{text: "Evans POST", poster: "Evan Altman", pic: "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Penguin-512.png", date : 4, likeCount : 10, comments : []}]}
	]};

	var currentUser = undefined;
    var profileUser = undefined;
    
	service.getUsers = function() {

		return service.model.people;
	}
	
	service.getUserNames = function() {
		var names = [];
		service.model.people.forEach(function(item) {
			var x = item.username;
			names.push(x)
		});
		return names;
	}
	
	service.setCurrentUser = function(user) {
		currentUser = user;
        profileUser = user;
	}
   
	
	service.getCurrentUser = function() {
        if(currentUser != undefined){
			return currentUser;
        }else{
            console.log("User was undefined");
            $location.path("/");
        }
	}

    service.setProfileUser = function(user) {
		profileUser = user;
	}
   
	
	service.getProfileUser = function() {
			return profileUser;
	}

    service.getFriends = function() {
        return _.filter(service.model.people, function(currentObject){
            return currentObject != currentUser;
        });
    }
	
    service.getDate = function() {
        return Math.round(new Date().getTime()/1000.0);
    }
    service.getTimeDifferenceString = function(oldTime){
            var currTime = Math.round(new Date().getTime()/1000.0);
            var difftime = currTime - oldTime;
            console.log(difftime);
            if(difftime>24*3600){
                return (Math.floor(difftime/24*3600)).concat(" days ago.");
            } else if(difftime>3600){
                return (Math.floor(difftime/3600)).concat(" hours ago.");
            } else if(difftime>60){
                return (Math.floor(difftime/24*3600)).concat(" minutes ago.");
            } else {
                return difftime.concat(" seconds ago.");
            }
        } 
    service.getUser = function(name){
        return _.find(service.model.people, function(obj){
            return obj.realName === name;
        })
    }
    
	return service;
});
