var app = angular.module("survevyNetwork");
app.directive("commentDirective", function() {
    return {
    	restrict : "E",
    	replace : "true",
        templateUrl : "commentDirective.html",
        scope : {
        	comment : "=info"
        }
    };
});
