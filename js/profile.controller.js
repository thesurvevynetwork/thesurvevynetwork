mainApp = angular.module("survevyNetwork");

mainApp.controller("ProfileController", function($scope, $location, dataFactory) {
        $scope.currentUser = dataFactory.getCurrentUser($location);
        $scope.profileUser = dataFactory.getProfileUser();
        $scope.postText = "";
        $scope.editMode = false;
    
    
        $scope.makePost = function(){
            if($scope.currentUser==$scope.profileUser && $scope.postText.length>0){
                $scope.profileUser.posts.unshift({text: $scope.postText, poster: $scope.currentUser.realName, pic: $scope.currentUser.pictureUrl, date : $scope.getDate(), likeCount : 0, comments : []});
                $scope.postText = "";
            }
		}
                
        $scope.getCurrentPosts = function(){
            if(dataFactory.getCurrentUser() != undefined){
            return $scope.profileUser.posts;
        }
        }	
        
        $scope.changePicture = function(){
            if($scope.currentUser == $scope.profileUser){
                var newUrl = prompt("Please link a new picture.");
                if((newUrl == undefined || newUrl.match(/\.(jpeg|jpg|gif|png)$/) != null)){
                    $scope.profileUser.pictureUrl = newUrl;
                } else {
                    confirm("Invalid picture");
                }
            }
                
        }

        $scope.goToFeed = function(){
            $location.path("/feed");
        }
        
        $scope.editProfile = function(){
            if($scope.currentUser == $scope.profileUser){
                $scope.editMode = !$scope.editMode;
            }
        }       
        
        $scope.getDate = function(){
            return dataFactory.getDate();
        }
        
        $scope.getTimeDifferenceString = function(time){
            return dataFactory.getTimeDifferenceString(time);
        }
        
        
	}
);
