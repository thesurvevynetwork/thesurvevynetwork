mainApp = angular.module("survevyNetwork");

mainApp.controller("FeedController", function($scope, $location, dataFactory) {
    $scope.friends = dataFactory.getFriends();
    $scope.friendsPosts = [];
    _.forEach($scope.friends, function(user){$scope.friendsPosts = $scope.friendsPosts.concat(user.posts)});
    sortFriendsPosts();

    function sortFriendsPosts() {
		$scope.friendsPosts =_.sortBy($scope.friendsPosts, function(user){
			return -1*user.date;
		});

      }
    $scope.visitMyProfile = function(){
        dataFactory.setProfileUser(dataFactory.getCurrentUser($location));
        $location.path("/profile");

    }

    $scope.getDate = function(oldDate) {
    	return service.getTimeDifferenceString(oldDate);

    }
});
