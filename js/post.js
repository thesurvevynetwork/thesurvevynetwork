var app = angular.module("survevyNetwork");
app.directive("postDirective", function() {
    return {
    	restrict : "E",
    	replace : "true",
		templateUrl : "postDirective.html",
        scope : {

        	post : "=info",
        	
        },
        controller:  ['$scope', "dataFactory", "$location", function($scope, dataFactory, $location){
            $scope.viewFriendsProfile = function(friendsName){
                var person = dataFactory.getUser(friendsName);
                dataFactory.setProfileUser(person);
                $location.path("/profile");	
			}
			$scope.newComment = "";
			$scope.addNewComment = function() {
				if($scope.newComment.length>0){
					$scope.post.comments.push({text : $scope.newComment, date : dataFactory.getDate(), likeCount : 0});
					$scope.newComment = "";
				}
			}			
    }]

    };
});