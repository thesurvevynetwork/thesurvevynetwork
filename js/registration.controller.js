mainApp = angular.module("survevyNetwork");

mainApp.controller("registrationControl", function($scope, $location, dataFactory) {
		$scope.newUsername = "";
		$scope.newPassword = "";
		$scope.newPasswordConfirm = "";
		
		$scope.makeNewUser = function() {
			var names = dataFactory.getUserNames();
			if (_.includes(names,$scope.newUsername) || ($scope.newUsername == "" && $scope.newPassword == "" && $scope.newPasswordConfirm == "")) {
				console.log("Name already exists or empty strings were entered");
				//Stay on registration page
			}
			else {
				if ($scope.newPassword == $scope.newPasswordConfirm) {
					var users = dataFactory.getUsers();
					users.push({username: $scope.newUsername, password: $scope.newPassword, realName: $scope.newUsername, pictureUrl: "http://www.cs.hope.edu/~jipping/floatinghead/jipping3.gif", bioText: "No bio given", posts: []});
					$location.path("/");

				}				
				else {
					console.log("Passwords don't match");
					//Stay on registration page
				}
			}

		}
		
	}
);