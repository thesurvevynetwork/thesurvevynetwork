mainApp = angular.module("survevyNetwork");

mainApp.filter('timeSinceDateFilter', function() {
  return function(oldTime, testingTime) {
    if(!testingTime){
        var currTime = Math.round(new Date().getTime()/1000.0);
    } else {
        var currTime = testingTime;
    }
    var difftime = currTime - oldTime;
    if(difftime>=24*3600*365){
        return (Math.floor(difftime/(24*3600*365))).toString()+(" years ago.");
    } else if(difftime>=24*3600){
        return (Math.floor(difftime/(24*3600))).toString()+(" days ago.");
    } else if(difftime>=3600){
        return (Math.floor(difftime/3600)).toString()+(" hours ago.");
    } else if(difftime>=60){
        return (Math.floor(difftime/60)).toString()+(" minutes ago.");
    } else if(isNaN(difftime) || difftime < 0){
        return "A while a go."
    }
    return difftime+(" seconds ago.");
  };
});
