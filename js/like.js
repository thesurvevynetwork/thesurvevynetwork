var app = angular.module("survevyNetwork");
app.directive("likeDirective", function() {
    return {
    	restrict : "E",
    	replace : "true",
		templateUrl : "likeDirective.html",
        scope : {
        	post : "=info"
        },
         controller:  ['$scope', function($scope){
            $scope.likeThisPost = function(post){
			     post.likeCount = post.likeCount + 1;
		  }
    }]
    };
});
