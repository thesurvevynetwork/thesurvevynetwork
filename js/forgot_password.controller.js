mainApp = angular.module("survevyNetwork");

mainApp.controller("forgotPasswordEmailControl", function($scope, $location, forgotPasswordEmailService) {
	$scope.emailAddress = "";
	
	$scope.sendPasswordEmail = function() {
		var address = $scope.emailAddress;
		if (address.indexOf('@')>= 1 && address.indexOf('.')>=3) {
			forgotPasswordEmailService.sendEmail($scope.emailAddress);
			$location.path("/");
		}
		else {
			console.log("invalid address");
		}
	}
});