mainApp = angular.module("survevyNetwork");

mainApp.controller("loginController", function($scope, $location, dataFactory) {
		$scope.currUsername = "";
		$scope.currPassword = "";

        dataFactory.setCurrentUser(undefined);
		$scope.login = function(){
			var users = dataFactory.getUsers();
			dataFactory.setCurrentUser(_.find(users, function(o){
				return (o.username == $scope.currUsername) && (o.password == $scope.currPassword);}));
		  if(dataFactory.getCurrentUser() != undefined){
            $location.path("/feed");
          }
        }
	
	}
);
